;;; init-feeds.el --- News feed (RSS/Atom) subscription support  -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2024  Chris Montgomery

;; Author: Chris Montgomery <chris@cdom.io>
;; Keywords: news, local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; But do I really want to know what's happening outside of Emacs?

;; FIXME: use <https://github.com/skeeto/elfeed>
;; FIXME: OPML instead of weird lists

;;; Code:

;; (require 'ceamx-user)

;; (use-feature! [newsticker]
;;   (setopt newsticker-url-list ceamx-news-feed-url-list))



(provide 'init-news)
;;; init-news.el ends here
