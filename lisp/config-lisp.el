;;; config-lisp.el --- Lispy variable definitions  -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2024  Chris Montgomery

;; Author: Chris Montgomery <chris@cdom.io>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Definitions relating to Emacs Lisp and other supported lispy languages.

;; Support indicated by `ceamx-lisp-modes-list'.

;;; Code:

(defvar ceamx-lisp-modes-list '(emacs-lisp-mode
                                ielm-mode
                                lisp-mode
                                inferior-lisp-mode
                                lisp-interaction-mode)
  "Supported Lisp modes.")

(defvar +emacs-lisp-outline-regexp "[ \t]*;;;\\(;*\\**\\) [^ \t\n]"
  "Regexp to use for `outline-regexp' in `emacs-lisp-mode'.
This marks a foldable marker for `outline-minor-mode' in elisp buffers.")

(provide 'config-lisp)
;;; config-lisp.el ends here
