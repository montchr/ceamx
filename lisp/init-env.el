;;; init-env.el --- Environment configuration -*- lexical-binding: t -*-

;; Copyright (c) 2022-2024  Chris Montgomery <chris@cdom.io>

;; Author: Chris Montgomery <chris@cdom.io>
;; URL: https://git.sr.ht/~montchr/ceamx
;; Version: 0.1.0

;; This file is NOT part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;  Ensure proper integration with the user environment.

;;; Code:

(require 'elpaca-autoloads)

(require 'config-env)

(require 'lib-common)

;; Disable unnecessary OS-specific command-line options.
(unless +sys-mac-p
  (setq command-line-ns-option-alist nil))
(unless +sys-linux-p
  (setq command-line-x-option-alist nil))

(elpaca exec-path-from-shell
  (require 'exec-path-from-shell)
  (dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "NIX_SSL_CERT_FILE" "NIX_PATH"))
    (add-to-list 'exec-path-from-shell-variables var))
  (exec-path-from-shell-initialize))

;;; Make temporary buffers inherit buffer-local environment variables with `inheritenv'

;; <https://github.com/purcell/inheritenv>

(after! 'exec-path-from-shell
  (elpaca inheritenv
    (require 'inheritenv)))

;;; Support integration with Direnv via the `envrc' package

;; <https://github.com/purcell/envrc>
;; <https://direnv.net/>
;; <https://github.com/direnv/direnv>

;; > Q: How does this differ from `direnv.el`?
;;
;; > <https://github.com/wbolster/emacs-direnv> repeatedly changes the global
;; > Emacs environment, based on tracking what buffer you're working on.
;;
;; > Instead, `envrc.el` simply sets and stores the right environment in each
;; > buffer, as a buffer-local variable.

(after! 'exec-path-from-shell
  (elpaca envrc
    (envrc-global-mode)))

(elpaca-wait)

(provide 'init-env)
;;; init-env.el ends here
