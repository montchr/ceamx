;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((nix-mode . ((mode . nix-format-nixfmt-on-save)))
 (org-mode . ((tab-width . 8))))
